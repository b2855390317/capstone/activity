import zuitt.com.example.Contact;
import zuitt.com.example.Phonebook;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Phonebook myPhonebook = new Phonebook();

        Scanner details = new Scanner(System.in);


        if (Phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {

            for (Contact contact : Phonebook.getContacts()) {
                System.out.println(contact.toString());
            }
        }


        System.out.println("Enter the details of the first contact:");

        System.out.print("Name: ");
        String name1 = details.nextLine();

        System.out.print("Contact number: ");
        String contactNumber1 = details.nextLine();

        System.out.print("Address: ");
        String address1 = details.nextLine();

        Contact myContact1 = new Contact();

        myContact1.setName(name1);
        myContact1.setContactNumber(contactNumber1);
        myContact1.setAddress(address1);

        myPhonebook.addContact(myContact1);

        System.out.println("Enter the details of the second contact:");

        System.out.print("Name2: ");
        String name2 = details.nextLine();

        System.out.print("Contact number: ");
        String contactNumber2 = details.nextLine();

        System.out.print("Address: ");
        String address2 = details.nextLine();


        Contact myContact2 = new Contact();

        myContact2.setName(name2);
        myContact2.setContactNumber(contactNumber2);
        myContact2.setAddress(address2);

        System.out.println(myContact1.getName());
        System.out.print("----------------------" + "\n" + myContact1.getName() + " has the following registered numbers:" + "\n");
        System.out.println(myContact1.getContactNumber());
        System.out.println(myContact2.getContactNumber());
        System.out.println("-----------------------");

        System.out.println(myContact2.getName() + " has the following registered addresses:");
        System.out.println("my home is in " + myContact1.getAddress());
        System.out.println("my home is in " + myContact2.getAddress());
        System.out.println("==============================");


        myPhonebook.addContact(myContact2);


    }
}