package zuitt.com.example;

public class Contact {

        private String name;
        private String contactNumber;
        private String address;


        public Contact() {
            this.name = "";
            this.contactNumber = "";
            this.address = "";
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
}
