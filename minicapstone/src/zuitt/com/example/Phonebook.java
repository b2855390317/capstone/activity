package zuitt.com.example;

import java.util.ArrayList;

public class Phonebook {
    public Phonebook() {
        contacts = new ArrayList<Contact>();
    }

    private static ArrayList<Contact> contacts;


    public static ArrayList<Contact> getContacts() {
        return contacts;
    }


    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    
    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }
}
